import { Provider } from 'react-redux'
import { createStore } from 'redux'
import reducers from './reducers'

function Root ({children, initialState={}}) {
    return(
        <Provider store={createStore(reducers, initialState)}>
        {children}
        </Provider>
    )
}

export default Root;