import CommentBox from "../CommentBox";
import { mount, shallow } from 'enzyme'
import Root from '../../Root'

let wrapped;
beforeEach(() => {
    wrapped = mount(
        <Root>
            <CommentBox />
        </Root>
    );
})

afterEach(() => {
    wrapped.unmount()
})

it('has a text area and a button', () => {
    // console.log(wrapped.find('textarea').debug());
    // console.log(wrapped.find('button'));

    expect(wrapped.find('textarea').length).toEqual(1)
    expect(wrapped.find('button').length).toEqual(1)
})

describe('the textarea', () => {
    beforeEach(() => {
        wrapped.find('textarea').simulate('change', {
            target: { value: 'new comment' }
        })
        wrapped.update()
    })
    it('has a text area that users can type in', () => {
        expect(wrapped.find('textarea').prop('value')).toEqual('new comment')
    })

    it('when submit form, textare gets emptied', () => {
        wrapped.find('form').simulate('submit');
        wrapped.update()
        expect(wrapped.find('textarea').prop('value')).toEqual('');
    })
})

