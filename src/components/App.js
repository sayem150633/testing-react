import React from 'react'
import CommentBox from "./CommentBox";
import CommentList from "./CommentList";

const App = () => {
    return (
        <div style={{textAlign:"center"}}>
            <CommentBox/>
            <CommentList/>
        </div>
    )
}

export default App