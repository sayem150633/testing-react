import commentReducer from '../../reducers'
import {SAVE_COMMENT} from '../../actions/types'

it('handle action of type save comments', ()=> {
    const action = {
        type: SAVE_COMMENT,
        payload: 'New Comment'
    }
    const newState = commentReducer([], action)
    expect(newState).toEqual({"comments": ["New Comment"]})
})

it('handle action of type unknown', ()=> {
    const newState = commentReducer([], {type: "JHGFRTYHH"})
    expect(newState).toEqual({"comments": []})
})
