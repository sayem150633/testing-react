import {SAVE_COMMENT} from '../actions/types'
// const initialState = {
//     comment: []
// }

export default function (state=[], action) {
    switch(action.type){
        case SAVE_COMMENT:
            return [...state, action.payload]
            // return Object.assign({}, state, {comment: action.payload})
        default:
            return state;
    }
}
